import { Icon, Paper, Typography } from "@mui/material";


function Dataset(props) {

    return <Paper elevation={3} style={{display: 'flex', flexDirection: 'column', padding: '10px', width: '190px', minWidth: '190px', margin: '0 5px'}}>
        <div style={{borderRadius: '50%', width: '60px', height: '60px', background: props.dataset.color, margin: '0 auto', display: 'flex'}}>
            <Icon style={{margin: 'auto'}} sx={{ fontSize: 30 }}>
                {props.dataset.icon}
            </Icon>
        </div>
        <Typography variant='caption' style={{textAlign: 'center', marginTop: '10px'}}>
            {props.dataset.name}
        </Typography>
    </Paper>
}

export default Dataset