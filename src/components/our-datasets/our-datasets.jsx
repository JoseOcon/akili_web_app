import { Paper } from "@mui/material"
import Dataset from "./dataset"


function OurDatasets() {

    const datasets = [
        { name: "Clasificar si a una persona sufrirá un ataque cerebro-vascular", icon: 'local_hospital', color: '#6DDDD6' },
        { name: "Clasificar el tipo de Hepatitis C de una persona", icon: 'local_hospital', color: '#6DDDD6' },
        { name: "Clasificar la estapa de cirrosis de una persona", icon: 'local_bar', color: '#873333' },
        { name: "Predecir la cantidad de pacientes recuperados de COVID 19", icon: 'coronavirus', color: '#49eb46' },
        { name: "Predecir el índice de masa corporal de una persona", icon: 'accessibility', color: '#dbc9a2' },
        { name: "Clasificar si a una persona cambiará de compañía telefónica", icon: 'contact_phone', color: '#5e6cff' },
        { name: "Predecir el precio del aguacate", icon: 'attach_money', color: '#6DDD71' },
        { name: "Predecir el precio del bitcoin", icon: 'attach_money', color: '#6DDD71' },
        { name: "Predecir el precio del S&P", icon: 'attach_money', color: '#6DDD71' },
        { name: "Predecir el valor de las ventas de la compañía Rossman", icon: 'attach_money', color: '#6DDD71' },
    ]

    return <Paper style={{ width: '100%', padding: '20px', maxHeight: '200px', minHeight: '200px', display: 'flex', overflowX: 'auto', background: '#32323280' }} elevation={5}>
        {datasets.map((dataset, i) => <Dataset dataset={dataset} key={i}></Dataset>)}
    </Paper>
}

export default OurDatasets