import { MainContextComponent } from "../../contexts/MainContext";
import { Fab, Icon, Tooltip } from "@mui/material";
import Instructions from "../instructions/instructions";
import Microphone from "../microphone/microphone";

function MainSection() {

  return (
    <MainContextComponent>
      <div style={{ display: "flex", height: "calc(100vh - 200px)" }}>
        <Tooltip title="Realizar análisis de sentimientos">
          <Fab
            style={{
              margin: "0 auto",
              width: "50px",
              height: "50px",
              top: "50px",
              right: "20px",
              position: "absolute",
            }}
            onClick={() => window.location.href = '/face-classificator'}
          >
            <Icon>photo_camera_front</Icon>
          </Fab>
        </Tooltip>
        <Instructions></Instructions>
        <Microphone></Microphone>
      </div>
    </MainContextComponent>
  );
}

export default MainSection;
