import { Backdrop, CircularProgress, Fab, Icon, Typography } from "@mui/material"
import hark from "hark"
import { useContext, useEffect, useLayoutEffect, useState } from "react"
import { say } from "../../common/speaker"
import { MainContext } from "../../contexts/MainContext"
import RecognitionService from "../../services/recognition-service"
import Circle from "./circle"


function Microphone() {

    const mainContext = useContext(MainContext)

    const [circleSizes, setCircleSizes] = useState([215, 235, 270])

    
    useLayoutEffect(() => {
        say("Hola, soy Akili,, ¿en qué puedo ayudarte?")
    }, [])

    const expandCircles = () => {
        setCircleSizes([220, 245, 290])
    }

    const normalCircles = () => {
        setCircleSizes([215, 235, 270])
    }



    /////// LOGIC

    const [recording, setRecording] = useState(false)
    const [speaking, setSpeaking] = useState(false)

    const [circlesInterval, setCirclesInterval]  = useState(null)

    useEffect(() => {
        if(recording && speaking){
            expandCircles()
            setCirclesInterval(setInterval(() => {
                expandCircles()
                setTimeout(() => {
                    normalCircles()
                }, 400)
            }, 300))
        }
    }, [recording, speaking])

    useEffect(() => {
        if(!recording || !speaking){
            console.log("interval", circlesInterval)
            clearInterval(circlesInterval)
            setCirclesInterval(null)
            normalCircles()
        }
    }, [recording, circlesInterval, speaking])


    const [mediaRecorder, setMediaRecorder] = useState(null)


    const micClick = () => {
        if (!recording) {
            startRecording()
        }
        else {
            stopRecording(mediaRecorder)
        }
    }


    const startRecording = () => {
        navigator.mediaDevices.getUserMedia({ audio: true })
            .then(stream => {
                console.log("Iniciando grabación")
                setRecording(true)

                let mediaRecorder1 = new MediaRecorder(stream);
                mediaRecorder1.start();

                setMediaRecorder(mediaRecorder1)



                const audioChunks = []

                mediaRecorder1.addEventListener("dataavailable", event => {
                    console.log("Chunk guardado")
                    audioChunks.push(event.data);
                });


                const speechEvents = hark(stream, {});

                let speaking = true
                let timeout = null

                speechEvents.on('speaking', function () {
                    console.log('speaking')
                    if(timeout)
                        clearTimeout(timeout)
                    speaking = true
                    setSpeaking(true)
                });

                speechEvents.on('stopped_speaking', function () {
                    speaking = false
                    setSpeaking(false)
                    console.log('stopped_speaking')
                    timeout = setTimeout(() => {
                        console.log("Tres segundos después", speaking)
                        if (!speaking) {
                            stopRecording(mediaRecorder1)
                        }
                    }, 3000)
                });

                mediaRecorder1.addEventListener("stop", () => {
                    console.log("Eliminando tracks")
                    setRecording(false)

                    stream.getTracks().forEach(track => track.stop())
                    let audioBlob1 = new Blob(audioChunks);
                
                    recognizeAudio(audioBlob1)
                });

            });
    }

    const stopRecording = (mediaRecorderParam) => {
        if (mediaRecorderParam != null) {
            mediaRecorderParam.stop()
        }
        setMediaRecorder(null)
    }

    const recognizeAudio = (audioBlob1) => {
        mainContext.setLoading(true)
        const previous_result = mainContext.response? {model: mainContext.response.result.model, params: mainContext.response.result.params}: undefined
        RecognitionService.sendAudio(audioBlob1, previous_result).then(response => {
            if(response.ok){
                mainContext.setResponse(response.response)
            }
            mainContext.setLoading(false)
        })
    }


    return <div style={{ width: '40%', margin: 'auto auto auto 0', display: 'flex', position: 'relative', flexDirection: 'column' }}>
        <Typography style={{ width: '100%', textAlign: 'center' }}>Dime lo que quieres saber y los datos que tienes</Typography>
        <Fab style={{
            margin: '0 auto', width: '200px', height: '200px', marginTop: '80px', position: 'relative',
            backgroundColor: recording ? '#ff9898' : '', transition: 'background-color 0.5s'
        }}
            onMouseEnter={expandCircles} onMouseLeave={normalCircles} onClick={micClick}>
            <Icon sx={{ fontSize: '100px' }}>
                micro
            </Icon>
            {circleSizes.map((circle, i) =>
                <Circle size={circle + 'px'} originalSize='200px' key={i}></Circle>
            )}
        </Fab>

        <Backdrop
            sx={{ color: '#fff', zIndex: '1000000'}}
            open={mainContext.loading}
        >
            <CircularProgress color="inherit" />
        </Backdrop>
    </div>
}

export default Microphone