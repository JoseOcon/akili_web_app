
function Circle(props) {

    return <div style={{
        position: 'absolute', 
        borderRadius: '50%', 
        width: props.size, 
        height: props.size, 
        top: `calc(calc(${props.size} - ${props.originalSize}) / -2)`, 
        left: `calc(calc(${props.size} - ${props.originalSize}) / -2)`, 
        background: 'transparent', 
        border: '2px solid #8080807a',
        transition: 'width 0.2s cubic-bezier(0, 0.93, 0.38, 0.6) 0s, height 0.2s cubic-bezier(0, 0.93, 0.38, 0.6) 0s, top 0.2s cubic-bezier(0, 0.93, 0.38, 0.6) 0s, left 0.2s cubic-bezier(0, 0.93, 0.38, 0.6) 0s'
    }}></div>
}

export default Circle