import { FaceClassificatorContextComponent } from "../../contexts/FaceClassificatorContext"
import CameraContainer from "./camera-container"
import FeelingsInformation from "./feelings-information"
import { Fab, Icon } from "@mui/material";

function FaceClassificator() {

    return <FaceClassificatorContextComponent>
        <div style={{ width: '100vw', height: '100vh', display: 'flex', flexDirection: 'row', backgroundColor: '#00000078', justifyContent: 'space-between' }}>
            <CameraContainer></CameraContainer>
            <FeelingsInformation></FeelingsInformation>
        </div>
        <Fab style={{ position: 'absolute', top: '10px', left: '10px' }} onClick={() => window.location.href = '/'}>
            <Icon>arrow_back</Icon>
        </Fab>
    </FaceClassificatorContextComponent>
}

export default FaceClassificator