import { Button, Paper, Typography } from "@mui/material"
import { useContext } from "react"
import { FaceClassificatorContext } from "../../contexts/FaceClassificatorContext"

function FeelingsInformation() {

    const faceClassificatorContext = useContext(FaceClassificatorContext)

    // anger: 0.003
    // contempt: 0.001
    // disgust: 0.001
    // fear: 0
    // happiness: 0.024
    // neutral: 0.957
    // sadness: 0.014
    // surprise: 0.001
    let feelings = {}

    if (faceClassificatorContext.imageResponse && faceClassificatorContext.imageResponse.response.ok) {

        feelings = {
            neutral: faceClassificatorContext.imageResponse.response.response.detected_faces[0].face_emotions.neutral,
            enojo: faceClassificatorContext.imageResponse.response.response.detected_faces[0].face_emotions.anger,
            desprecio: faceClassificatorContext.imageResponse.response.response.detected_faces[0].face_emotions.contempt,
            disgusto: faceClassificatorContext.imageResponse.response.response.detected_faces[0].face_emotions.disgust,
            miedo: faceClassificatorContext.imageResponse.response.response.detected_faces[0].face_emotions.fear,
            felicidad: faceClassificatorContext.imageResponse.response.response.detected_faces[0].face_emotions.happiness,
            tristeza: faceClassificatorContext.imageResponse.response.response.detected_faces[0].face_emotions.sadness,
            sorpresa: faceClassificatorContext.imageResponse.response.response.detected_faces[0].face_emotions.surprise,
        }
    }

    const getFrame = () => {
        if (faceClassificatorContext.imageCapture != null) {
            faceClassificatorContext.imageCapture.takePhoto().then(faceClassificatorContext.onFrameRetrieved)
        }
    }



    return <Paper style={{ margin: 'auto', width: '300px', maxHeight: '80%', minHeight: '400px', display: 'flex', padding: '20px', flexDirection: 'column', overflowY: 'auto' }}>
        <Typography variant='h6'>Análisis de sentimientos</Typography>
        <Typography variant='caption'>Se necesita una cara en la cámara para obtener los sentimientos</Typography>
        <br />
        <Button variant='contained' onClick={getFrame}>Analizar frame</Button>

        <br />

        <Typography variant='subtitle1'>Resultados</Typography>

        {Object.entries(feelings).map(feeling => (
            <Typography variant='body2'>{feeling[0].toUpperCase()}: <b>{feeling[1] * 100}%</b></Typography>
        ))}
    </Paper>
}

export default FeelingsInformation