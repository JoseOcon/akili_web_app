import { useContext, useLayoutEffect } from "react"
import { FaceClassificatorContext } from "../../contexts/FaceClassificatorContext"


function CameraContainer(props) {

    const { imageResponse, setImageCapture } = useContext(FaceClassificatorContext)

    useLayoutEffect(() => {
        navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
            const video = document.getElementById('video')
            video.srcObject = stream
            video.play()

            setImageCapture(new window.ImageCapture(stream.getTracks()[0]))
        })
    }, [setImageCapture])


    return <div style={{ margin: 'auto', width: 'calc(70vw - 300px)', height: '0', paddingBottom: 'calc(calc(70vw - 300px) * 0.67)', maxHeight: '90vh', position: 'relative' }}>
        <video id="video"  style={{ width: '100%'}}></video>
        <img id="image" style={{width: imageResponse ? '30%' : '100%', position: 'absolute', top: '0', right: '0', zIndex: imageResponse? '1': '-1', transition: 'width 0.5s' }} alt="Frame" src={imageResponse ? URL.createObjectURL(imageResponse.image) : null} />
    </div>
}

export default CameraContainer