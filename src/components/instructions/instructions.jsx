import { Paper } from "@mui/material"
import { useContext } from "react"
import { MainContext } from "../../contexts/MainContext"
import Back from "./back"
import MainInstructions from "./main-instructions"
import Model from "./model"
import NoModel from "./no-model"

function Instructions() {

    const mainContext = useContext(MainContext)


    return <Paper elevation={5} style={{margin: 'auto', padding: '20px', maxWidth: '40%', maxHeight: '80%', overflowY: 'auto', minWidth: '300px'}}>
        {!mainContext.response? <MainInstructions/>: null}
        {mainContext.response && (!mainContext.response.result || !mainContext.response.result.model)? <NoModel text={mainContext.response.text.toLowerCase()}/>: null}
        {mainContext.response && mainContext.response.result && mainContext.response.result.model? <Model/>: null}

        {mainContext.response? <Back></Back>: null}
    </Paper>
}

export default Instructions