import { Fab, Icon, Tooltip } from "@mui/material";
import { useContext } from "react";
import { MainContext } from "../../contexts/MainContext";

export default function Back() {

    const mainContext = useContext(MainContext)

    const onBack = () => {
        mainContext.setResponse(null)
    }

    return <Tooltip title="Atrás (todo se borrará)">
        <Fab style={{ position: 'absolute', top: '10px', left: '10px' }} onClick={onBack}>
            <Icon>arrow_back</Icon>
        </Fab>
    </Tooltip>
}