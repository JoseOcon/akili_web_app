import { Typography } from "@mui/material";

export default function MainInstructions() {
    return <>
        <Typography variant='h6'>Hola, soy <b style={{color: '#9aff9d'}}>AKILI</b></Typography>
        <Typography variant='h6'>¿Cómo entiendo?</Typography>

        <br />

        <Typography variant='body2'>Puedes decirme</Typography>
        <Typography variant='h6' style={{ color: '#FFE27B', fontWeight: '300' }}>"Quiero saber el precio del aguacate"</Typography>
        <Typography variant='body2'>y yo te diré cuáles datos necesito</Typography>

        <br />

        <Typography variant='body2'>Para darme datos, dime "datos" y luego cada uno</Typography>
        <Typography variant='h6' style={{ color: '#80FFE8', fontWeight: '300' }}>"<b>datos</b> la temperatura es 37"</Typography>
        <Typography variant='body2'>o "la temperatura es igual a 37"</Typography>

        <br />



        <Typography variant='body2'>Siempre para darme más de un argumento,
            <br />necesito que los separes con una "y", así</Typography>
        <Typography variant='h6' style={{ color: '#FF84F3', fontWeight: '300' }}>"<b>datos</b> X es 5 <b>y</b> Z es calor"</Typography>
    </>
}