import { Chip, Tooltip, Typography } from "@mui/material";
import { useContext, useEffect, useState } from "react";
import Swal from "sweetalert2";
import models, { getModelResponse } from "../../common/models";
import { say } from "../../common/speaker";
import { MainContext } from "../../contexts/MainContext";
import ParamChip from "./param-chip";


export default function Model() {

    const mainContext = useContext(MainContext)

    const [params, setParams] = useState(mainContext.response.result.params)

    console.log(params)
    console.log(mainContext.response.result.errors)

    useEffect(() => {
        if(mainContext.response.result.response !== null) {
            const modelResponse = getModelResponse(mainContext.response.result.model, Number(mainContext.response.result.response))
            Swal.fire(
                modelResponse.title,
                modelResponse.message,
                'success'
            )
            say(modelResponse.title)
        }
    }, [mainContext.response.result, mainContext.response.result.model])

    useEffect(() => {
        setParams(mainContext.response.result.params)
    }, [mainContext.response])

    useEffect(() => {
        say("Estamos " + models[mainContext.response.result.model])
    }, [mainContext.response.result.model])

    useEffect(() => {
        if(Object.keys(mainContext.response.result.errors).length > 0)
            say("Hay algunos errores en los datos que me diste, revísalos")
    }, [mainContext.response.result.errors])

    useEffect(() => {
        if(Object.keys(mainContext.response.result.missing).length > 0)
            say("Hay algunos datos que me faltan, fíjate")
    }, [mainContext.response.result.missing])

    const handleDelete = (param) => {
        setParams(prev => {
            delete prev[param[0]]
            return { ...prev }
        })
    }

    return <>
        <Typography variant='h6'>{models[mainContext.response.result.model]}</Typography>

        <br />

        <Typography variant='body1'>
            Me diste estos datos
            {Object.keys(mainContext.response.result.errors).length > 0 ? <b style={{ color: '#ff9292' }}> (hay algunos errores en los datos)</b> : null}
        </Typography>

        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {Object.entries(params).map(param =>
                <ParamChip param={param} resultObject={mainContext.response.result} onDelete={handleDelete} key={param[0]}></ParamChip>
            )}
        </div>

        <br />


        {Object.keys(mainContext.response.result.missing).length > 0 ? (<>
            <Typography variant='body1'>
                Faltan estos datos
            </Typography>
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {Object.entries(mainContext.response.result.missing).map(param =>
                    <Tooltip title={param[1]} >
                        <Chip label={param[0].toUpperCase()} key={param[0]} style={{margin: '5px', backgroundColor: 'white', color: 'black'}}></Chip>
                    </Tooltip>
                )}
            </div>
        </>) : null}
    </>
}