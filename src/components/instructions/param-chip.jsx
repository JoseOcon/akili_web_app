import { Chip, Tooltip } from "@mui/material";


export default function ParamChip(props) {

    const errors = props.resultObject.errors
    const recognized = props.resultObject.recognized

    const getParamState = () => {

        if(errors[props.param[0]] !== undefined){
            return 1
        }
        else if (recognized[props.param[0]] !== undefined) {
            return 0
        }
        return 2
    }
    
    const state = getParamState()


    const getTooltipTitle = () => {
        if(state === 1) {
            return "ERROR: " + errors[props.param[0]]
        }
        else if (state === 0) {
            return "Dato válido"
        }
        else{
            return "Este dato no es necesario para el modelo"
        }
    }

    const getParamStateColor = () => {

        if(state === 1){
            return '#8d0000'
        }
        else if (state === 0) {
            return 'green'
        }
        return ''
    }

    return <Tooltip title={getTooltipTitle()}>
        <Chip
        key={props.param[0]}
        style={{ margin: '5px', backgroundColor: getParamStateColor() }}
        label={`${props.param[0].toUpperCase()} = ${props.param[1].replace("uno", "1")}`}
        onDelete={() => props.onDelete(props.param)}
        />
        </Tooltip>
}