import { Typography } from "@mui/material";
import { useLayoutEffect } from "react";
import { say } from "../../common/speaker";

export default function NoModel(props) {

    useLayoutEffect(() => {
        say("Disculpa, no te he entendido")
    }, [props.text])


    return <>
        <Typography variant='h6'>Disculpa, no te he entendido</Typography>

        <br />
        <Typography variant='body1'>Te entendí esto <b>"{props.text}"</b> y no coincide con ninguno de mis modelos</Typography>
    </>
}