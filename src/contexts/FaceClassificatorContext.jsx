import React from "react"
import FaceClassificatorService from "../services/face-classificator-service"

export const FaceClassificatorContext = React.createContext(null)

export function FaceClassificatorContextComponent({children}) {
    const [imageResponse, setImageResponse] = React.useState(null)

    const [getFrame, setGetFrame] = React.useState(null)

    const [imageCapture, setImageCapture] = React.useState(null)

    const onFrameRetrieved = async (frame) => {
        const response = await FaceClassificatorService.sendImage(frame)
        setImageResponse({image: frame, response})
    }

    const value = {
        imageResponse,
        setImageResponse,
        getFrame,
        setGetFrame,
        onFrameRetrieved,
        imageCapture, 
        setImageCapture
    }

    return <FaceClassificatorContext.Provider value={value}>
        {children}
    </FaceClassificatorContext.Provider>
}


