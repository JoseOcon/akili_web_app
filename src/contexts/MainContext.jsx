import React from "react"

export const MainContext = React.createContext(null)

export function MainContextComponent({children}) {
    const [response, setResponse] = React.useState(null)
    const [loading, setLoading] = React.useState(false)

    const value = {
        response,
        setResponse,
        loading,
        setLoading
    }

    return <MainContext.Provider value={value}>
        {children}
    </MainContext.Provider>
}


