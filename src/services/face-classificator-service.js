import APIUtils from "../common/api-utils"

const API_URL = "http://localhost:5000"


export default class FaceClassificatorService {

    static async sendImage(imageBlob) {
        const formData = new FormData()

        if(imageBlob){
            formData.append('image', imageBlob)
        }
        
        try {
            const response = await APIUtils.POST(`${API_URL}/face`, formData)
            if(response.ok){
                return {ok: true, response: await response.json()}
            }
            else{
                return {ok: false, response: response}
            }
        } catch (error) {
            return {ok: false, error: error}
        }
    } 
}