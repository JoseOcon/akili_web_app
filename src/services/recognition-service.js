import APIUtils from "../common/api-utils"

const API_URL = "http://localhost:5000"


export default class RecognitionService {

    static async sendAudio(audioBlob, info) {
        const formData = new FormData()

        if(info){
            formData.append('json', JSON.stringify(info))
        }

        if(audioBlob){
            formData.append('audio', audioBlob)
        }
        
        try {
            const response = await APIUtils.POST(`${API_URL}`, formData)
            if(response.ok){
                return {ok: true, response: await response.json()}
            }
            else{
                return {ok: false, response: response}
            }
        } catch (error) {
            return {ok: false, error: error}
        }
    } 
}