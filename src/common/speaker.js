
import Swal from 'sweetalert2'
import '@sweetalert2/theme-dark'

const speaker = new window.SpeechSynthesisUtterance()
speaker.lang = 'es'

let confirmed = null

window.speechSynthesis.onvoiceschanged = () => {
    const esVoices = window.speechSynthesis.getVoices().filter(voice => voice.lang.includes('es-'))

    const sabina = esVoices.find(voice => voice.name.includes("Sabina"))

    if (sabina) {
        speaker.voice = sabina
    }
}

export async function say(text) {
    speaker.text = text

    if (confirmed === null) {
        const result = await Swal.fire({
            title: '¿Quieres que te hable?',
            text: "Te hablaré para que entiendas mejor lo que estoy procesando",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí'
        })

        if (result.isConfirmed) {
            confirmed = true
        }
        else {
            confirmed = false
        }

    }

    if (confirmed === true) {
        window.speechSynthesis.speak(speaker)
    }
}