
const models = {
    'stroke': "Clasificando si una persona sufrirá un ataque cerebro vascular",
    'telcom': "Clasificando si una cambiará de compañía telefónica",
    'cirrhosis': "Clasificando la etapa de la cirrosis de un paciente",
    'covid': "Prediciendo la cantidad de pacientes recuperados de Covid19",
    'rossman': "Prediciendo las ventas de la compañía Rossman",
    'btc': "Prediciendo el precio del Bitcoin",
    'avocado': "Prediciendo el precio del Aguacate",
    "standard_and_poors": "Prediciendo el índice de Standard And Poor's 500",
    'imc': 'Prediciendo el índice de masa corporal de una persona',
    'hepatitis': 'Prediciendo el tipo de Hepatitis C de un paciente (si es que posee)'
}

export default models

export const getModelResponse = (model, value) => {
    let title = `El valor es ${value}`
    let message = `Los datos indican que para el modelo ${model} el valor es ${value}`

    if(model === 'stroke'){
        title = `La persona ${value? 'sí': 'no'} sufrirá un ataque cerebro-vascular`
        message = `El modelo indica que la persona con los datos dados ${value? 'sí': 'no'} sufrirá un ataque cerebro-vascular`
    }
    else if(model === 'telcom'){
        title = `La persona ${value? 'sí': 'no'} se cambiará de campañía telefónica`
        message = `El modelo indica que la persona con los datos dados ${value? 'sí': 'no'} cambiará su compañía telefónica`
    }
    else if(model === 'cirrhosis'){
        title = `El paciente tiene una cirrosis en etapa ${value}`
        message = `El modelo indica que la persona con los datos dados tiene una cirrosis en etapa ${value}`
    }
    else if(model === 'covid'){
        title = `Predicción de Pacientes Recuperados`
        message = `La cantidad de recuperados será de:  ${Math.round(value)}`
    }
    else if(model === 'rossman'){
        title = `Predicción de Ventas de Rossman`
        message = `El total de ventas será de  $${Math.round(value)}`
    }
    else if(model === 'btc'){
        title = `Predicción de Precio del Bitcoin`
        message = `El modelo indica que el precio del Bitcoin será de: $${Math.round(value)}`
    }
    else if(model === 'avocado'){
        title = `Predicción del Precio del Aguacate`
        message = `El modelo indica que el precio del Aguacate será de: $${value.toFixed(2)}`
    }
    else if(model === 'standard_and_poors'){
        title = `Predicción del índice del Standard & Poor's 500`
        message = `El modelo indica que el índice del Standard & Poor's 500 es: ${value.toFixed(2)}`
    }
    else if(model === 'imc'){
        title = `Predicción del Índice de Masa Corporal`
        message = `El IMC será de ${Math.round(value)}`
    }
    else if(model === 'hepatitis'){
        title = `Predicción del Tipo de Hepatitis C`
        message = `El paciente ${value === 0 ? 'no posee hepatitis, es donante sano' : value === 1 ? 'posee hepatitis aguda' : value === 2 ? 'posee fibrosis hepática' :  value === 3 ? 'posee hepatitis crónica': 'es un donante sospechoso'}`
    }



    return {title, message}

}