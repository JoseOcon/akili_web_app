import MainSection from './components/main-section/main-section';
import OurDatasets from './components/our-datasets/our-datasets';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import FaceClassificator from './components/face-classificator/face-classificator';



function App() {
  return (
    <Router>
        <Switch>
          <Route path="/face-classificator">
            <FaceClassificator></FaceClassificator>
          </Route>
          <Route path="/">
            <div style={{width: '100vw', height: '100vh', display: 'flex', flexDirection: 'column', backgroundColor: '#00000078'}}>
              <MainSection></MainSection>
              <OurDatasets></OurDatasets>
            </div>
          </Route>
        </Switch>
    </Router>
  );
}

export default App;
